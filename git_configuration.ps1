<#
.SYNOPSIS
    This is a simple git configuration example file

.DESCRIPTION
    For use with new users or as a reference on how to check, configure and use git.
    PLEASE MAKE SURE GIT IS INSTALLED https://git-scm.com/ OR WITH CHOCOLATEY ON WINDOWS OR GIT FOR UNIX/LINUX USING YOUR PACKAGE MANAGER.
    
    For Cmder, Posh-Git, Oh-My-Posh & Powerline Fonts see https://gist.github.com/jchandra74/5b0c94385175c7a8d1cb39bc5157365e

    A good beginner tutorial: https://ihatetomatoes.net/git-tutorial-for-beginners/

    Make sure to create your SSH key pair to push and pull: https://docs.gitlab.com/ee/ssh/README.html#generating-a-new-ssh-key-pair
    Make sure to add a personal access token: https://gitlab.com/-/profile/personal_access_tokens
    For this project, the personal access token is: HyUnvyCq-E9mbqsCPfag

    Get started with terminology and general basics: https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html

    For rollingback changes read: https://opensource.com/article/18/6/git-reset-revert-rebase-commands
.PARAMETER DemoParam1
    none.

.EXAMPLE
    All examples are below.

.NOTES
    Author: Michael J. Acosta
    Last Edit: 12/9/2020
    Version 1.0 - initial release 

#>


#Check for git version and if its installed.
git --version


#Configure git name and email
git config --global user.name "Full Name"
git config --global user.email "username@domain.com"


#Check using the list parameter
git status
git config --global --list


#Init repository
git init
#or init in a subfolder instead
git init git_project


#Stage the files for commit
git add .\git_configuration.ps1
#Discard changes
git restore .\git_configuration.ps1


#Commit your first changes
git commit -m "another test commit"

#Check log for status, completion, others
git log

#Oh no! I want to revert my local changes
git reset .\git_configuration.ps1

#Pushing to remote repository(Make sure to create or use exisint url.git from GitLab)
git remote add origin git@gitlab.com:mikeyjoel/git_configuration.git

#Pulling from repository (edit on the server/webide and commit - recommended before pulling for testing)
git pull https://gitlab.com/mikeyjoel/git_configuration.git master

#Check URLS stored for remote
git remote -v

#Create Branches
git checkout -b newfeature
#More on branches here: https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging

#Checkout local branch and integrate remote branch
git checkout master
git pull master master

#Cloning repository
git clone #url.git
